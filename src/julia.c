/*
  Modified serial version of L10 Mandelbrot assignment for Julia Set
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "png_util.h"

void julia(const int NRe, 
		const int NIm, 
	   	const float minRe,
	   	const float minIm,
		float * h_count){

  int n,m;
  float newzRe, newzIm, oldzRe, oldzIm;

  // set C values here
  float cRe = minRe;    // real c component
  float cIm = minIm;    // imaginary c component

  // Iterate through every pixel in grid
  for(m=0;m<NIm;++m){
    for(n=0;n<NRe;++n){

      // compute initial real and imaginary components of z
      // f(z) = z^2 + c
      // z1 = 0 --> z = c
      // z2 = (c^2) + c
      // c^2 = (a+bi)^2 = a^2 + abi + abi + b^2*i^2 = a^2-b^2 + 2abi
      // scale to (4096, 4096) pixel grid with interval -2 to 2
      newzRe = 2.0*(n-NRe/2)/(0.5*NRe);    // real z component
      newzIm = (m-NIm/2)    /(0.5*NIm);    // imaginary z component
     
      
      int Nt = 200;
      int t, cnt=0;
      for(t=0;t<Nt;++t){
	oldzRe = newzRe;
	oldzIm = newzIm;
	
	// z = z^2 + c
	//   = (zRe + i*zIm)*(zRe + i*zIm) + (cRe + i*cIm)
	//   = zRe^2 - zIm^2 + 2*i*zIm*zRe + cRe + i*cIm
        newzRe = oldzRe*oldzRe - newzIm*newzIm + cRe;     // z=a^2-b^2 + c
	newzIm = 2.f*oldzIm*oldzRe + cIm;                 // z=2*a*b*i + c

	float jul = newzRe*newzRe+newzIm*newzIm;
	if (jul > 4.f) break; // check if diverging to infinity
 
	// if within bounds, add value to array and continue 
	cnt += (jul<4.f);
      }

     
      h_count[n + m*NRe] = cnt;
    }
  }
  

}


int main(int argc, char **argv){

  const int NRe = 4096;
  const int NIm = 4096;

  /* box containing sample points */
  //const float centRe = -0.759856, centIm= 0.125547;
  //const float diam  = 0.151579;
  //const float minRe = centRe-0.5*diam;
  //const float remax = centRe+0.5*diam;
  //const float minIm = centIm-0.5*diam;
  //const float immax = centIm+0.5*diam;

  //const float dRe = (remax-minRe)/(NRe-1.f);
  //const float dIm = (immax-minIm)/(NIm-1.f);
  const float minRe = -0.8;
  const float minIm = 0.156;
  
  float *h_count = (float*) calloc(NRe*NIm, sizeof(float));

  double tic = clock();

  // call mandelbrot from here
  julia(NRe, NIm, minRe, minIm, h_count);

  double toc = clock();

  double elapsed = (toc-tic)/CLOCKS_PER_SEC;
  
  printf("elapsed time %g\n", elapsed);

  FILE *png = fopen("julia.png", "w");
  write_hot_png(png, NRe, NIm, h_count, 0, 80);
  fclose(png);

}
