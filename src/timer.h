#include <time.h>
#include <sys/time.h>

// use high resolution timer
// see: http://www.guyrutenberg.com/2007/09/22/profiling-code-using-clock_gettime/

timespec getHostTime();
timespec getHostElapsedTime(timespec start, timespec end);
long getHostElapsedNanoseconds(timespec start, timespec end);
long getHostElapsedResolution();
