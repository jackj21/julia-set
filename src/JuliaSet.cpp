// JuliaSet.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


/*
*  Computes values for Julia Set based on Mandelbrot in class code.
*  
*/
void juliaSerial(const int nRe, const int nIm, const float minRe, const float minIm, const float dRe, const float dIm, float* h_count) {
    for (int i=0; i<nIm; i++) {
        for (int j = 0; j < nRe; j++) {
            float cRe = minRe + j * dRe;
            float cIm = minIm + i * dIm;

            float zRe = 0;
            float zIm = 0;

            int its = 1000;
            int count = 0;

            for (int it = 0; it < its; it++) {
                // z = z^2 + c
                //   = (zRe + i*zIm)*(zRe + i*zIm) + (cRe + i*cIm)
                //   = zRe^2 - zIm^2 + 2*i*zIm*zRe + cRe + i*cIm
                float zRetmp = zRe * zRe - zIm * zIm + cRe;
                zIm = 2.f * zIm * zRe + cIm;
                zRe = zRetmp;

                count += zRe * zRe + zIm * zIm + cRe;
            }
            h_count[j + i*nRe] = count;
        }
    }

}


int main()
{
    
}



