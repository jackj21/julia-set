/*

L10 in class assignment code slightly modified for Julia Set


*/

#include <stdio.h>
#include <stdlib.h>
#include <timer.h>

extern "C"
{
#include "png_util.h"
}

__global__ void juliaKernelV0(const int NRe, 
				   const int NIm, 
				   const float minRe,
				   const float minIm,
				   float * __restrict__ c_count){

  // 2D array of threads in each thread-block
  int m = threadIdx.y + blockIdx.y*blockDim.y;
  int n = threadIdx.x + blockIdx.x*blockDim.x;  
  float newzRe, newzIm, oldzRe, oldzIm;

  // assign real and imaginary components of c
  float cRe = minRe;
  float cIm = minIm;
  
  if(m<NIm && n<NRe){

    // scale components of z to be within x=[-2,2]
    newzRe = 1.5*(n-NRe/2)/(0.5*NRe);
    newzIm = (m-NIm/2)    /(0.5*NIm);
    int Nt = 400;
    int t, cnt=0;

    // compute iterative function and check forescape or prisoner bound
    for(t=0;t<Nt;++t){
      oldzRe = newzRe;
      oldzIm = newzIm;
      // z = z^2 + c
      //   = (zRe + i*zIm)*(zRe + i*zIm) + (cRe + i*cIm)
      //   = zRe^2 - zIm^2 + 2*i*zIm*zRe + cRe + i*cIm
      newzRe = oldzRe*oldzRe - newzIm*newzIm + cRe; // real z=a^2-b^2 + real c
      newzIm = 2.f*oldzIm*oldzRe + cIm;             // im z=2*a*b*i + im c

      float jul = newzRe*newzRe+newzIm*newzIm;
      if (jul > 4.f) break; // escape
      
      cnt += (jul<4.f);     // prisoner
    }
    
    c_count[n + m*NRe] = cnt;
  }
}

/**
__global__ void juliaKernelV1(const int Nre, 
				   const int Nim, 
				   const float minRe,
				   const float minIm,
				   const float dRe, 
				   const float dIm,
				   float * __restrict__ c_count){
  
  int m = threadIdx.y + blockIdx.y*blockDim.y;
  int n = threadIdx.x + blockIdx.x*blockDim.x;  

  if(m<Nim && n<Nre){
    float cRe = minRe + n*dRe; // fmad
    float cIm = minIm + m*dIm; // fmad
    
    float zRe = 0;
    float zIm = 0;
    
    int Nt = 400;
    int t=0, cnt=0;
    for(t=0;t<Nt;++t){
      
      // z = z^2 + c
      //   = (zRe + i*zIm)*(zRe + i*zIm) + (cRe + i*cIm)
      //   = zRe^2 - zIm^2 + 2*i*zIm*zRe + cRe + i*cIm
      float zReTmp = cRe + zRe*zRe; // FMAD
      float zImTmp = zIm*zRe; // the odd duck
      zRe = zReTmp - zIm*zIm; // FMAD
      zIm = cIm + 2.f*zImTmp; // FMAD

      float dist = -4.f + zRe*zRe; // FMAD
      dist += zIm*zIm; // FMAD

      // it is cheaper to not do flops [ although this can break unrolling ]
      // however, we only save if all threads in the warp break here
//      if(dist>=0) break;
      cnt += (dist>0);
    }
    
    c_count[n + m*Nre] = cnt;
  }
}
**/

void runJuliaKernel(const int NRe, const int NIm, const float minRe, const float minIm, int kernel){

  // set up storage
  float *h_count = (float*) calloc(NRe*NIm, sizeof(float));
  float *c_count;
  cudaMalloc(&c_count, NRe*NIm*sizeof(float));

  // set up events
  cudaEvent_t tic, toc;
  cudaEventCreate(&tic);
  cudaEventCreate(&toc);

  // define thread array
  int NX = 32, NY=8;
  dim3 G( (NRe + NX-1)/NX, (NIm + NY-1)/NY, 1);
  dim3 B( NX, NY, 1);
  
  cudaEventRecord(tic);

  if(kernel==0)
     juliaKernelV0 <<< G, B >>> (NRe, NIm, minRe, minIm, c_count);
  /*if(kernel==1)
     juliaKernelV1 <<< G, B >>> (NRe, NIm, minRe, minIm, dRe, dIm, c_count);
  */
  
  cudaEventRecord(toc);

  // this forces HOST<>DEVICE sync
  cudaMemcpy(h_count, c_count, NRe*NIm*sizeof(float), cudaMemcpyDeviceToHost);

  float elapsed;
  cudaEventElapsedTime(&elapsed, tic, toc);
  elapsed /= 1000;
  
  printf("%e; %%%% kernel %d elapsed time\n", elapsed, kernel);
  
  if(1){
    char fileName[BUFSIZ];
    sprintf(fileName, "juliaKernel%d.png", kernel);
    FILE *png = fopen(fileName, "w");
    write_hot_png(png, NRe, NIm, h_count, 0, 80);
    fclose(png);
  }

  cudaFree(c_count);
  free(h_count);
}




int main(int argc, char **argv){

  const int NRe = 4096;
  const int NIm = 4096;

  /* box containing sample points */
  const float minRe = -0.4;
  const float minIm = 0.6;
  
  // call serial julia
  //printf("Running serial: \n");
  //runJuliaSerial(NRe, NIm, minRe, minIm, dRe, dIm);

  // call CUDA mandelbrot
  int kernel;

  // kernel 0
  // run a warm up
  printf("Running kernel 0: \n");
  runJuliaKernel(NRe, NIm, minRe, minIm, kernel=0);
  runJuliaKernel(NRe, NIm, minRe, minIm, kernel=0);

  /**
  // kernel 1
  // run a warm up
  printf("Running kernel 1: \n");
  runJuliaKernel(NRe, NIm, minRe, minIm, dRe, dIm, kernel=1);
  runJuliaKernel(NRe, NIm, minRe, minIm, dRe, dIm, kernel=1);
  **/
  
  return 0;
}
